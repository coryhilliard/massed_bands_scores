# Massed Bands Scores

In an effort to reduce the level of difficulty for beginner snare players, the Alliance of North American Pipe Band Associations (ANAPBA) has revised all of the massed bands drum scores.

I've created a more visually appealing set of these drum scores for snare drummers.

The original set can be found on the PPBSO web site located here:  

http://ppbso.org/wp-content/uploads/2021/11/Massed-Band-Scores_2021.pdf
